# Django-test

Just a test site where I can dump a bunch of random crap for testing and demo purposes.

## Running in Docker

If you have Docker and Docker Compose installed, or are willing to install it, you can simply run:

```bash
docker compose up --build
```

and the application should be available at http://localhost:8000/

If this port is otherwise occupied on your machine, you may change it in the `docker-compose.yml` file.

If your version of either Docker or Docker Compose is older, you may need to run `docker-compose` with a dash instead of the space.

## Running in a virtual environment

```bash
# Clone the application
git clone https://gitlab.com/theepic-dev/django-test
cd django-test
# Create a virtualenv and install requirements
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
# Apply migrations
python manage.py migrate
# If this is the first time running the server, create a superuser
python manage.py createsuperuser
# Run the application
python manage.py runserver
```

The application should be available at http://localhost:8000/
