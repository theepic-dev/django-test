from django.shortcuts import render

from .models import counter


def get_counter(request):
    """Get view."""
    return render(request, "counter/index.html", {"count": counter.count})


def increment_counter(request):
    """Increment view."""
    # For a real use, this should check the CSRF token and only be triggered on POST
    # but as this is a simple demo app, a get with no checks is fine
    counter.count += 1
    return render(request, "counter/increment_partial.html", {"count": counter.count})
