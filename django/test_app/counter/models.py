"""Models for the counter app."""


class Counter:
    """Singleton counter."""

    def __init__(self):
        """Init method."""
        self.count = 0


# Instantiate the counter instance to keep global state
counter = Counter()
