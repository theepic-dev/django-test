from django.db import models


class Component(models.Model):
    """A component used to craft items."""

    name = models.CharField(max_length=255)
    weight = models.FloatField()

    def __str__(self):
        return self.name


class Item(models.Model):
    """Some item."""

    name = models.CharField(max_length=255)
    components = models.ManyToManyField("Component", through="ItemComponent")

    def __str__(self):
        return self.name


class ItemCharacteristics(models.Model):
    """Some item characteristics."""

    item = models.OneToOneField("Item", on_delete=models.CASCADE)
    is_shiny = models.BooleanField()
    is_new = models.BooleanField()

    class Meta:
        verbose_name = "Item characteristic"
        verbose_name_plural = "Item characteristics"

    def __str__(self):
        return f"{self.item.name} characteristics"


class ItemComponent(models.Model):
    """A join table between items and components."""

    item = models.ForeignKey("Item", on_delete=models.CASCADE)
    component = models.ForeignKey("Component", on_delete=models.CASCADE)
    quantity = models.PositiveSmallIntegerField(help_text="How many of this component does the item require?")
