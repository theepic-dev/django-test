import logging

from celery import shared_task

logger = logging.getLogger(__name__)


@shared_task
def test_task():
    """Random celery test."""

    logger.info("Test task ran")
