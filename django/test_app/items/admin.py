from django.contrib import admin

from .models import Component, Item, ItemCharacteristics, ItemComponent

admin.site.register(Component)
admin.site.register(Item)
admin.site.register(ItemCharacteristics)
admin.site.register(ItemComponent)
